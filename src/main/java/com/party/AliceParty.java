package com.party;

import java.util.Scanner;

public class AliceParty {
    private static final int N;     //N - number of all guests
    static {
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.print("Enter number of guests: ");
            String s = scan.next();
            if(s.matches("[0-9]+")){
                N = Integer.parseInt(s);
                break;
            } else {
                System.out.println("Input integer number please!");
            }
        }
    }

    /*
     * Method for calculating probability that everyone at the party will hear the rumor -
     * product of all probabilities
     */
    public static double getProbability(int n) {    //n - number of guests
        if (N < 3) return 1;
        if (n == 0 || n == 1) {
            return (1. / (N - 2));
        } else {
            return (((double) n / (N - 2)) * getProbability(n - 1));
        }
    }

    /*
     * Method for calculating expected number of people to hear the rumor -
     * sum of all probabilities + 2 (Bob and person after him)
     */
    public static double getExpectedNumber(int n) {
        if (N < 4) return N;
        if (n == 0 || n == 1) {
            return (1. / (N - 2));
        } else {
            return (2. / (N-3) + ((double) n / (N - 2)) + getExpectedNumber(n - 1));
        }
    }

    public static void main(String[] args) {
        System.out.println("Probability that everyone at the party will hear the rumor = " + getProbability(N-2));
        System.out.println("Expected number of people to hear the rumor = " + Math.round(getExpectedNumber(N-2)));
    }
}
